﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHitbox : MonoBehaviour
{
    EnemyClass myParent;
    //ParentAI myParent;
    // Start is called before the first frame update
    void Start()
    {
        myParent = transform.parent.GetComponent<EnemyClass>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.CompareTag("PiniataWeapon"))
        {
            myParent.health--;
            myParent.GetHit();
            Debug.Log("Ouch!!!");
        }
    }
}
