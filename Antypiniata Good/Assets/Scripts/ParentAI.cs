﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParentAI : EnemyClass
{
    public int attackValue = 2;
    public float movementSpeed = 2;
    public int counter;

    bool attacking, chasing;

    void Start()
    {
        counter = 0;
        attacking = false;
        chasing = false;

    }

    void Update()
    {
        if (counter == 1)
        {
            Debug.Log(transform.name + " gonię!");
            transform.position = Vector3.MoveTowards(transform.position, PlayerController.Instance.transform.position, movementSpeed * Time.deltaTime);
        } else if (counter == 2)
        {
            Debug.Log(transform.name + " atakuję!");
        } 
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            Debug.Log("Kolizja z graczem");
            counter++;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            counter--;
        }
    }
}
