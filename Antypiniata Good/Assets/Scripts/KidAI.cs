﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KidAI : EnemyClass {

    public GameObject homeSpot;
    public int attackValue;
    public float speed, escapeSpeed;

    bool goHome;
    SpriteRenderer spriteRenderer;
    ObjectPooler parentsPooler;
    ObjectPooler candyPooler;
    Vector3 prevPosition;
    float timer = 0;
    float delay = 2;

    float attackTimer = 0, attackFrequency = 1;


    Transform playerTransform;
    int counter = 0;

	void Start () {
        playerTransform = GameObject.Find("Player").transform;
        goHome = false;
        spriteRenderer = GetComponent<SpriteRenderer>();
        parentsPooler = GameObject.Find("ParentsPooler").GetComponent<ObjectPooler>();
        candyPooler = GameObject.Find("CandyPooler").GetComponent<ObjectPooler>();
        prevPosition = new Vector3(-100, -100, 0);
    }
	
	void Update () {



        if (!goHome && PlayerController.Instance.insideGround)
        {
            if (counter == 1)
            {
                transform.position = Vector2.MoveTowards(transform.position, playerTransform.position, speed * Time.deltaTime);
                if (playerTransform.position.x < transform.position.x)
                {
                    transform.localEulerAngles = new Vector3(0, 180, 0);
                } else
                {
                    transform.localEulerAngles = new Vector3(0, 0, 0);
                }
            }
            else if (counter == 2)
            {
                attackTimer += Time.deltaTime;
                if(attackTimer >= 1f / attackFrequency)
                {
                    PlayerController.Instance.healthValue -= attackValue;
                    PlayerController.Instance.AS.clip = PlayerController.Instance.getHitClip;
                    PlayerController.Instance.AS.Play();
                    attackTimer = 0;
                }
            }
        }

        if (health <= 0)
        {
            GameObject candy = candyPooler.GetPooledObject();
            if (candy != null)
            {
                homeSpot.transform.parent.GetChild(0).GetComponent<Animator>().SetBool("GetOut", false);
                candy.transform.position = new Vector3(transform.position.x, transform.position.y - spriteRenderer.bounds.size.y / 4, 0);
                candy.SetActive(true);
                Destroy(this.gameObject);
            }
        }

        if (health == 1 && homeSpot != null)
        {
            if (prevPosition != transform.position)
            {
                prevPosition = transform.position;
                transform.position = Vector2.MoveTowards(transform.position, homeSpot.transform.position, escapeSpeed * Time.deltaTime);
                if (homeSpot.transform.position.x < transform.position.x)
                {
                    transform.localEulerAngles = new Vector3(0, 180, 0);
                }
                else
                {
                    transform.localEulerAngles = new Vector3(0, 0, 0);
                }
                goHome = true;
            }
            else
            {
                timer += Time.deltaTime;
                homeSpot.transform.parent.GetChild(0).GetComponent<Animator>().SetBool("GetOut", true);
                if (timer >= delay)
                {
                    GameObject parent = parentsPooler.GetPooledObject();
                    if (parent != null)
                    {
                        parent.transform.position = homeSpot.transform.position;
                        parent.SetActive(true);
                        homeSpot.transform.parent.GetChild(0).GetComponent<Animator>().SetBool("GetOut", false);
                        Destroy(this.gameObject);
                    }
                    timer = 0;
                }
                
            }

        }

	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player") {
            counter++;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            counter--;
        }
    }
}
