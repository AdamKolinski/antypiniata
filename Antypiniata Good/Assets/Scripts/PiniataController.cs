﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PiniataController : MonoBehaviour
{

    public float movementSpeed = 4, jumpForce = 5, jumpPos;

    Vector2 moveDirection;
    Rigidbody2D rb;
    public bool freeMovement = false, inAir = false, jumpFromInside;
    public Collider2D currentlyColliding;
    Collider2D pinataCol;
    SpriteRenderer spriteRenderer;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        pinataCol = GetComponent<Collider2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        HandleMovement();

        Jump();



    }

    void HandleMovement()
    {
        if (jumpPos > -100f)
        {
            if (transform.position.y < jumpPos)
            {
                inAir = false;
                freeMovement = true;
                rb.gravityScale = 0;
                jumpPos = -100;
                jumpFromInside = false;
                spriteRenderer.sortingLayerID = 10;
            }
        }

        moveDirection = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

        if (moveDirection.x > 0)
        {
            transform.localEulerAngles = new Vector3(0, 0, 0);
        }
        else if (moveDirection.x < 0)
        {
            transform.localEulerAngles = new Vector3(0, 180, 0);
        }

        if (freeMovement == true)
        {
            rb.gravityScale = 0;
            //if (inAir == false) rb.isKinematic = true;
            currentlyColliding.isTrigger = true;
            rb.velocity = moveDirection * movementSpeed;

        }
        else
        {

            rb.gravityScale = 1;
            if (currentlyColliding != null && inAir == false) currentlyColliding.isTrigger = false;
            rb.velocity = new Vector2(moveDirection.x * movementSpeed, rb.velocity.y);
            if (moveDirection.y < 0 && currentlyColliding.transform.tag == "Ground" && inAir == false)
            {
                freeMovement = true;
                Debug.Log("Peniiiiis");
            }
        }


    }

    void Jump()
    {
        if (Input.GetKeyDown(KeyCode.Space) && inAir == false)
        {
            //Physics2D.IgnoreLayerCollision(8, 9, true);
            pinataCol.isTrigger = true;

            if (freeMovement == false)
            {
                rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
                inAir = true;
                spriteRenderer.sortingLayerID = 12;
            }
            else
            {
                jumpFromInside = true;
                rb.gravityScale = 1;
                freeMovement = false;
                jumpPos = transform.position.y;
                rb.velocity = new Vector2(rb.velocity.x, 0);
                rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
                inAir = true;
            }

        }

        if (inAir == false)
        {
            //Physics2D.IgnoreLayerCollision(8, 9, false);
            pinataCol.isTrigger = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Ground")
        {
            if (jumpFromInside == false) freeMovement = true;

        }
    }

    private void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == "Ground")
        {
            if (inAir == false)
            {
                freeMovement = false;
                rb.velocity = new Vector2(rb.velocity.x, 0);
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D col)
    {

        if (col.transform.tag != "Enemy") currentlyColliding = col.transform.GetComponent<Collider2D>();

        if (col.transform.tag == "Ground")
        {
            if (jumpFromInside == false) inAir = false;
        }
        if (col.transform.tag == "Obstacle")
        {
            inAir = false;
            freeMovement = false;
        }
    }

    private void OnCollisionExit2D(Collision2D coll)
    {
        if (coll.transform.tag == "Ground")
        {
            currentlyColliding = null;
        }
    }
}
