﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CandyController : MonoBehaviour
{
    public float speed;
    Image candyImage;
    Text candyText;
    bool catchCandy;
    Animator anim;
    Vector3 prevPosition;

    void Start()
    {
        candyImage = GameObject.Find("CandyImage").GetComponent<Image>();
        candyText = GameObject.Find("CandyText").GetComponent<Text>();
        anim = GetComponent<Animator>();
        prevPosition = new Vector3(-100, -100, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (catchCandy)
        {
            if (prevPosition != transform.position)
            {
                if (anim != null) anim.enabled = false;
                prevPosition = transform.position;
                transform.position = Vector3.MoveTowards(transform.position, Camera.main.ScreenToWorldPoint(candyImage.transform.position), speed * Time.deltaTime);
            }
            else
            {
                PlayerController.Instance.points++;
                candyText.text = PlayerController.Instance.points.ToString();
                this.gameObject.SetActive(false);
            }

        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            catchCandy = true;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            catchCandy = true;
        }
    }
}
