﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyClass : MonoBehaviour
{
    public int health;
    public AudioSource AS;
    public AudioClip getHitClip;

    public void GetHit()
    {
        AS.clip = getHitClip;
        AS.Play();
    }
}
