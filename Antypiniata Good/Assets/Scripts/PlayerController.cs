﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static PlayerController Instance;

    public int points = 0;
    public int healthValue = 10;
    public float movementSpeed = 3;
    public float jumpForce = 4;
    public AudioSource AS;
    public AudioClip getHitClip;

    float jumpPositionY;
    Collider2D playerCollider;
    Collider2D combatCollider;
    public SpriteRenderer spriteRenderer;
    public GameObject shadow;
    Vector2 direction;
    Rigidbody2D rb;

    float attackTimer = 0;
    public float attackDuration = 0.5f;

    [HideInInspector]
    public bool insideGround = true, inAir = false;

    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        playerCollider = GetComponent<Collider2D>();
        combatCollider = transform.GetChild(0).GetComponent<Collider2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        rb.centerOfMass = Vector2.zero;
    }

    // Update is called once per frame
    void Update()
    {
        direction = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        HandleMovement();

        if (Input.GetKeyDown(KeyCode.J))
        {
            if (!combatCollider.gameObject.activeSelf)
            {
                attackTimer = attackDuration;
                combatCollider.gameObject.SetActive(true);
                Debug.Log("Pinata attack");
            }
        }
        else
        {
            attackTimer -= Time.deltaTime;
            if (attackTimer <= 0)
            {
                combatCollider.gameObject.SetActive(false);
            }

            //rb.centerOfMass = Vector2.zero;
        }

    }

    void HandleMovement()
    {
        if (direction.x > 0)
        {
            transform.localEulerAngles = new Vector3(0, 0, 0);
        }
        else if (direction.x < 0)
        {
            transform.localEulerAngles = new Vector3(0, 180, 0);
        }

        if (transform.position.y <= jumpPositionY)
        {
            rb.velocity = new Vector2(rb.velocity.x, 0);
            rb.gravityScale = 0;
            Physics2D.IgnoreLayerCollision(8, 10, false);
            playerCollider.isTrigger = false;
            inAir = false;
            spriteRenderer.sortingOrder = 10;
            jumpPositionY = -100;
        }

        if (insideGround)
        {

            if (inAir == false)
            {
                rb.velocity = new Vector2(direction.x * movementSpeed, direction.y * movementSpeed);
                shadow.transform.position = new Vector3(transform.position.x, transform.position.y - 0.03f, 0);
            }
            else
            {
                rb.velocity = new Vector2(direction.x * movementSpeed, rb.velocity.y);
                shadow.transform.position = new Vector3(transform.position.x, shadow.transform.position.y, 0);
            }
        }
        else
        {
            rb.velocity = new Vector2(direction.x * movementSpeed, rb.velocity.y);
        }

        if (Input.GetKeyDown(KeyCode.Space)) Jump();
    }

    void Jump()
    {
        if (inAir == false)
        {
            if (insideGround)
            {
                inAir = true;
                Physics2D.IgnoreLayerCollision(8, 10);
                spriteRenderer.sortingOrder = 12;
                playerCollider.isTrigger = true;
                jumpPositionY = transform.position.y;
                rb.gravityScale = 1;
                rb.velocity = new Vector2(rb.velocity.x, 0);
                rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
            }
            else
            {
                inAir = true;
                rb.velocity = new Vector2(rb.velocity.x, 0);
                rb.AddForce(new Vector2(direction.x, jumpForce), ForceMode2D.Impulse);
            }
        }
    }

    void OnCollisionStay2D(Collision2D col)
    {
        if (col.transform.tag == "Ground" && direction.y < 0)
        {
            rb.gravityScale = 0;
            insideGround = true;
            shadow.gameObject.SetActive(true);
            col.gameObject.GetComponent<Collider2D>().isTrigger = true;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == 10)
        {
            inAir = false;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.layer == 10)
        {
            inAir = true;
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == "Ground" && inAir == false)
        {
            rb.gravityScale = 1;
            insideGround = false;
            col.isTrigger = false;
            rb.velocity = new Vector2(rb.velocity.x, 0);
            shadow.gameObject.SetActive(false);
        }
    }
}
