﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]
[RequireComponent(typeof(Renderer))]
public class IsometricObject : MonoBehaviour
{
   private const int IsometricRangePerYUnit = 100;
   
   [Tooltip("Will use this object to compute z-order")]
    public PlayerController TargetScript;
    public Transform Target;
    Renderer renderer;
    public float offset;
    bool active;

    private void Start()
    {
        TargetScript = GameObject.FindObjectOfType<PlayerController>() as PlayerController;
        renderer = GetComponent<Renderer>();
        Target = TargetScript.transform;
    }

    void Update()
    {
        active = TargetScript.inAir;
        //renderer.sortingOrder = -(int)(Target.position.y * IsometricRangePerYUnit) + TargetOffset;
        if (active == false) {
            if (Target.position.y - offset > transform.position.y) {
                renderer.sortingOrder = Target.GetComponent<SpriteRenderer>().sortingOrder + 1;
            } else {
                renderer.sortingOrder = Target.GetComponent<SpriteRenderer>().sortingOrder - 1;
            }
        }
    }
}